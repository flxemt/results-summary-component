import data from './data.json'

const categories = document.getElementById('categories')
const scoreEl = document.getElementById('total-score')

function render() {
  const totalScore = data.length * 100
  let scoreSum = 0
  let html = ''

  data.forEach(item => {
    scoreSum += item.score
    html += `
      <div class="categories__item" style="background-color: ${item.background}">
        <div class="item__left" style="color: ${item.foreground}">
          <img src="${item.icon}" alt="${item.category} icon" />
          <span>${item.category}</span>
        </div>
        <div class="item__right">${item.score} <span class="score-left">/ 100</span></div>
      </div>
    `
  })

  categories.innerHTML = html
  const averageScore = Math.floor((scoreSum / totalScore) * 100)
  scoreEl.textContent = averageScore
}

render()
